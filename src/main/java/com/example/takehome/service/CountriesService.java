package com.example.takehome.service;

import com.example.takehome.exception.InvalidCountryCodeException;
import com.example.takehome.response.ContinentListResponse;
import com.example.takehome.response.ContinentResponse;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;

@Service
@Slf4j
public class CountriesService {

    @Autowired
    CountryContinentDataService countryContinentDataService;

    private Set<String> countryCodes;
    private Map<String, String> countryContinentMap;
    private Map<String, LinkedHashSet<String>> continentCountryLinkedHashSetMap;

    @PostConstruct
    public synchronized void initialize() {
        countryContinentMap = countryContinentDataService.getCountryContinentMap();

        countryCodes = new HashSet<>();
        continentCountryLinkedHashSetMap = new HashMap<>();
        Function<String, LinkedHashSet<String>> createLinkedHashSet = k -> new LinkedHashSet<>();

        for (Map.Entry<String, String> entry : countryContinentMap.entrySet()) {
            String country = entry.getKey();
            String continent = entry.getValue();

            countryCodes.add(country);

            LinkedHashSet<String> countryCollection = continentCountryLinkedHashSetMap.computeIfAbsent(continent, createLinkedHashSet);
            countryCollection.add(country);
        }
    }

    // START: I synchronize them just in case they are used before their initialization has finished
    private synchronized Set<String> getCountryCodes() {
        return countryCodes;
    }

    private synchronized Map<String, String> getCountryContinentMap() {
        return countryContinentMap;
    }

    private synchronized Map<String, LinkedHashSet<String>> getContinentCountryLinkedHashSetMap() {
        return continentCountryLinkedHashSetMap;
    }
    // END: I synchronize them just in case they are used before their initialization has finished

    public void allAreValid(List<String> countries) {
        Set<String> countryCodes = getCountryCodes();
        for (String country : countries) {
            if (!countryCodes.contains(country)) {
                String exceptionMessage = String.format("%s is not a valid country code", country);
                throw new InvalidCountryCodeException(exceptionMessage);
            }
        }
    }

    public LinkedHashSet<String> getAllCountriesInContinent(String continentName) {
        return getContinentCountryLinkedHashSetMap().get(continentName);
    }

    public HashMap<String, String> countryListToCountryContinentMap(List<String> countries) {
        Map<String, String> allCountryContinentMap = getCountryContinentMap();
        HashMap<String, String> countryContinentMap = new HashMap<>();
        for (String country : countries) {
            countryContinentMap.put(country, allCountryContinentMap.get(country));
        }
        return countryContinentMap;
    }

    public ContinentListResponse generateContinentListResponse(List<String> countries) {
        allAreValid(countries);
        HashMap<String, String> countryContinentMap = countryListToCountryContinentMap(countries);

        ContinentListResponse responseList = new ContinentListResponse();
        responseList.setContinent(new ArrayList<>());

        Map<String, ContinentResponse> continentResponseMap = new LinkedHashMap<>();
        for (Map.Entry<String, String> entry : countryContinentMap.entrySet()) {
            String country = entry.getKey();
            String continent = entry.getValue();

            ContinentResponse response = continentResponseMap.get(continent);
            if (response == null) {
                response = new ContinentResponse();
                responseList.getContinent().add(response);
                continentResponseMap.put(continent, response);
            }

            response.setName(continent);

            List<String> responseCountries = response.getCountries();
            if (responseCountries == null) {
                responseCountries = new ArrayList<>();
                response.setCountries(responseCountries);
            }
            responseCountries.add(country);

            LinkedHashSet<String> otherCountries = response.getOtherCountries();
            if (otherCountries == null) {
                LinkedHashSet<String> countriesInContient = getAllCountriesInContinent(continent);
                otherCountries = new LinkedHashSet<>(countriesInContient);
                response.setOtherCountries(otherCountries);
            }
            otherCountries.remove(country);
        }

        return responseList;
    }

}
