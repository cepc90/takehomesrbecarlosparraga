package com.example.takehome.service;

import com.example.takehome.utils.PropertyFileLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;

@Slf4j
@Service
public class CountryContinentDataService {

    public Map<String, String> getCountryContinentMap() {
        try {
            return obtainFromRemoteServer();
        } catch (Exception e) {
            log.warn("Unable to update country continent map from the server. Falling back to default country code value property file");
            try {
                return obtainFromLocalPropertyFile();
            } catch (IOException ex) {
                log.error("Default country continent list not found. Application unstable. Fix the issue and redeploy");
                throw new RuntimeException(ex);
            }
        }
    }

    private Map<String, String> obtainFromRemoteServer() throws Exception {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String requestBody = "{\"query\":\" { countries { code continent { name } } } \"}";

        HttpEntity<String> request = new HttpEntity<>(requestBody, headers);

        String uri = "https://countries.trevorblades.com/graphql";
        ResponseEntity<Map> responseEntity = restTemplate.exchange(uri, HttpMethod.POST, request, Map.class);

        Map<String, Object> responseMap = responseEntity.getBody();
        return getCountryContinentMapFromResponse(responseMap);
    }

    @SuppressWarnings("unchecked")
    Map<String, String> getCountryContinentMapFromResponse(Map<String, Object> responseMap) {
        List<Map<String, Object>> countries = (List<Map<String, Object>>) ((Map<String, Object>) responseMap.get("data")).get("countries");

        Map<String, String> countryToContinentMap = new LinkedHashMap<>();
        for (Map<String, Object> country : countries) {
            String countryCode = (String) country.get("code");
            Map<String, Object> continent = (Map<String, Object>) country.get("continent");
            String continentName = (String) continent.get("name");
            countryToContinentMap.put(countryCode, continentName);
        }
        return countryToContinentMap;
    }

    private Map<String, String> obtainFromLocalPropertyFile() throws IOException {
        return PropertyFileLoader.loadProperties("country_continent.properties");
    }

}
