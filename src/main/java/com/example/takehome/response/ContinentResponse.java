package com.example.takehome.response;

import lombok.Data;

import java.util.LinkedHashSet;
import java.util.List;

@Data
public class ContinentResponse {

    List<String> countries;
    String name;
    LinkedHashSet<String> otherCountries;

}
