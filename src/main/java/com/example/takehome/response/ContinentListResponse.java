package com.example.takehome.response;

import lombok.Data;

import java.util.List;

@Data
public class ContinentListResponse {

    List<ContinentResponse> continent;

}
