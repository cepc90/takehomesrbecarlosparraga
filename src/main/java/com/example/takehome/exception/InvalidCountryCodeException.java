package com.example.takehome.exception;

public class InvalidCountryCodeException extends RuntimeException {

    public InvalidCountryCodeException(String message) {
        super(message);
    }

}
