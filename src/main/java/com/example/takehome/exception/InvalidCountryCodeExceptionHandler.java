package com.example.takehome.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class InvalidCountryCodeExceptionHandler {

    @ExceptionHandler(InvalidCountryCodeException.class)
    public ResponseEntity<String> handleInvalidCountryCodeException(InvalidCountryCodeException ex) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ex.getMessage());
    }

}
