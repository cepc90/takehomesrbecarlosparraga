package com.example.takehome.controllers;

import com.example.takehome.response.ContinentListResponse;
import com.example.takehome.service.CountriesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
public class ContinentController {

    @Autowired
    CountriesService countriesService;

    @GetMapping("/continents")
    ContinentListResponse continentsGet(@RequestParam List<String> countries) {
        return countriesService.generateContinentListResponse(countries);
    }

    @GetMapping("/continentsCsv")
    ContinentListResponse continentsGetCsv(@RequestParam String countries) {
        List<String> countriesList = Arrays.asList(countries.split(","));
        return countriesService.generateContinentListResponse(countriesList);
    }

}
