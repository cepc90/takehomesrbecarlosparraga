package com.example.takehome.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HelloWorldController {

    @GetMapping("/helloWorld")
    String helloWorld() {
        log.info("Hello world was called");
        log.warn("Believe me, it has been called");
        return "Hello";
    }

}
