package com.example.takehome.utils;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class PropertyFileLoader {

    public static Map<String, String> loadProperties(String fileName) throws IOException {
        Resource resource = new ClassPathResource(fileName);
        InputStream input = resource.getInputStream();
        Properties properties = new Properties();
        properties.load(input);

        Map<String, String> map = new HashMap<>();
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            map.put(key, value);
        }

        return map;
    }
}
