## Development Considerations

Since limiting requests per second was mentioned, I focused the developed logic into being as quick to respond as possible by keeping the information in memory and only loading it when the application starts. Maybe it could be improved upon further by reducing the number of loops but I played around with over 100 countries in a request and was getting responses in around 5ms.


## Version Considerations

As suggested by the **INSTRUCTIONS.md**, I used the latest version of spring, which at the time of this writting is **3.0.5** in Spring Initilizr (https://start.spring.io/;)

I used OpenJDK version 19 on my computer to build this project since version 17 of the OpenJDK was superseeded https://jdk.java.net/17/.

I used version 17 for "sourceCompatibility = '17'" in build.gradle since version 20 is not yet supported by gradle https://docs.gradle.org/current/userguide/compatibility.html. 

I used **gradle** instead of **maven** since the *starting point project* used gradle as well.

I used **application.yml** instead of **application.properties** since the *starting point project* used yml.

I used **amazoncorretto:19** as a parent docker image, since the openjdk image has a deprecation notice https://hub.docker.com/_/openjdk

## How to run

By using gradle
```
./gradlew clean bootRun
```

To generate an executable Jar
```
./gradlew clean bootJar
java -jar ./build/libs/takehome-0.0.1-SNAPSHOT.jar
```

By using Docker
```
./gradlew clean bootJar
docker build -t take_home_sr_be_carlos_parraga:0.0.1 .
docker run -p 8080:8080 take_home_sr_be_carlos_parraga:0.0.1
```

Example request:
```
localhost:8080/continents?countries=PR,VE,AG,AI
```
OR
```
localhost:8080/continents?countries=PR&countries=AG&countries=AI&countries=AW&countries=BB
```
