FROM amazoncorretto:19

WORKDIR /takeHomeSrBeCarlosParraga

COPY build/libs/takehome-0.0.1-SNAPSHOT.jar /takeHomeSrBeCarlosParraga/takehome-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java", "-jar", "/takeHomeSrBeCarlosParraga/takehome-0.0.1-SNAPSHOT.jar"]